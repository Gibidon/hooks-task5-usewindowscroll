import { useEffect, useState } from 'react'

export interface IScrollCoordinates {
  x?: number
  y?: number
}

function getScrollCoordinates(): IScrollCoordinates {
  return { x: window.scrollX, y: window.scrollY }
}

export function useWindowScroll() {
  const [scroll, setScroll] = useState<IScrollCoordinates>(() => getScrollCoordinates())

  function applyScrollCoordinates() {
    setScroll(getScrollCoordinates())
  }

  function scrollTo({ x, y }: IScrollCoordinates) {
    window.scrollTo({ left: x, top: y, behavior: 'smooth' })
    setScroll({ x: 0, y: 0 })
  }

  useEffect(() => {
    window.addEventListener('scroll', applyScrollCoordinates)

    return () => window.removeEventListener('scroll', applyScrollCoordinates)
  }, [])

  return { scroll, scrollTo }
}
