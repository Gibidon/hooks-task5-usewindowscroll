import { useWindowScroll } from './hooks/useWindowScroll'

export function App() {
  const { scroll, scrollTo } = useWindowScroll()

  return (
    <div className="flex justify-center">
      <ul>
        <h1 className="bg-blue-400">Top</h1>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <p>
          Scroll position x: {scroll.x}, y: {scroll.y}
        </p>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>

        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li>.</li>
        <li className="bg-blue-200">Bottom</li>
        <div>
          <button onClick={() => scrollTo({ y: 0 })} className="bg-red-200 border rounded p-4">
            Scroll to top
          </button>
        </div>
      </ul>
    </div>
  )
}
